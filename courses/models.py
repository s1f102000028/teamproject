from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils import timezone
from datetime import datetime
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
# Create your models here.

class instructors(models.Model):
    Id=models.AutoField(primary_key=True)
    profs_name=models.TextField()
    contact_mails=models.TextField()
    def __str__(self):
        return "%s" % (self.profs_name)

class courses_detail(models.Model):
    courses_name=models.TextField()
    courses_id=models.AutoField(primary_key=True)
    profs=models.ForeignKey(
        instructors,
        on_delete=models.CASCADE,
        verbose_name="profs_name",
        null=True)
    classroom_num=models.TextField(max_length=4)
    moocs_url=models.TextField(default="none")
    meets_url=models.TextField(default="none")
    toyo_url=models.TextField(default="none")
    year=models.IntegerField(validators=[MaxValueValidator(datetime.now().year),MinValueValidator(2017)])
    students=models.ManyToManyField(User)
    detail=models.TextField(default="")
    content=models.TextField(default="")
    prerequisite=models.TextField(default="")
class courses_slot(models.Model):
    course_detail=models.ForeignKey(
        courses_detail,
        on_delete=models.CASCADE,
        verbose_name="course_detail",
        null=True
    )
    slot=models.IntegerField(validators=[MaxValueValidator(30),MinValueValidator(1)])

#a='CS' (courses_detail type)
#b=courses_slot(course_detail=a, slot=4)
# courses_slot has 2 columns 
