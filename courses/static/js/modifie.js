var op=0;
window.onload=show;
function show(){
    setInterval(fading,50);
}
function fading(){
    var body=document.getElementById("body");
    op=Number(window.getComputedStyle(body).getPropertyValue("opacity"));
    if (op<1){
        op=op+0.1;
        body.style.opacity=op;
    }
    else{
        clearInterval(0);
    }
}
function add(article_id){
    var req= new XMLHttpRequest();
    req.onreadystatechange=function(){
        if ((req.readyState==4)&&(req.status==200)){
            var json=JSON.parse(req.responseText);
            callback(json);
        }
    }
    req.open('GET','/api/articles/'+article_id+'/like');
    req.send(null);
}