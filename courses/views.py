from django.shortcuts import render,redirect
from django.http import HttpResponse,Http404
from courses.models import courses_detail,courses_slot
from collections import OrderedDict
from django.contrib.auth.models import User
from django.urls import reverse
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from courses.forms import CustomUserCreationForm
# Create your views here.

def register(request):
    if request.method=="GET":
        return render(request, "courses/register.html",{"form": CustomUserCreationForm, "message": ""})
    elif request.method == "POST":
        if request.POST.get("submit")=="Login":
            return redirect("overview")
        else:
            form=CustomUserCreationForm(request.POST)
            if form.is_valid():
                user=form.save()
                login(request,user)
                return redirect("syllabus")
            else:
                return render(request,"courses/register.html",{"form": CustomUserCreationForm, "message": "username existed or password ensurement error. Please try again"})
def syllabus(request):
    if(request.user.is_authenticated):
        return redirect("overview")
    return render(request,"courses/syllabus.html")
# courses_detail page test:
#  
def course_detail(request, courses_id):
    try:
        course=courses_detail.objects.get(pk=courses_id)
    except courses_detail.DoesNotExist:
        raise Http404('Course does not exist')
    data = {
        'detail':course.detail,
        'content':course.content,
        'prerequisite': course.prerequisite,
        'id': course.courses_id,
        'course_name': course.courses_name,
        'professor': course.profs,
        'classroom': course.classroom_num,
        'year': course.year,
        'detail': course.detail,
    }
    return render(request, 'courses/courses_detail.html', data)


# Create your views here.
def overview(request):
    try:
        if(request.user.is_authenticated):
            taken=request.user.courses_detail_set.all()
        else:
            taken=[]
        cours=courses_detail.objects.all().order_by('courses_id')
        period=courses_slot.objects.all().order_by('slot')
        weekdays=["Monday", "Tuesday","Wednesday","Thursday","Friday"]
        courses_list={ 
        "cours": cours,
        "period": period,
        "week": weekdays,
        "taken": taken
        }
    except:
        raise Http404("Service under maintenance")
    return render(request,'courses/overview.html',courses_list)
@login_required(login_url="/login")
def add(request,courses_id):
    bag=request.user.courses_detail_set.all()
    desire=courses_detail.objects.get(pk=courses_id)
    period=courses_slot.objects.filter(course_detail=desire)
    check=True
    if desire not in bag:
        for item in bag:
            bagx=courses_slot.objects.filter(course_detail=item)
            if bagx.intersection(period):
                check=False
    else:
        check=False
    if check:
        desire.students.add(request.user)
        desire.save()
        return redirect("courses")
    else:
        return redirect("overview")
@login_required(login_url="/login")
def courses(request):
    person = request.user
    period = [0,1,2,3,4,5,6]
    course_list = OrderedDict()
    for p in period:
      course_list[str(p)] = {'p0' : '','p1':'','p2':'','p3' : '','p4':''}
    cours=person.courses_detail_set.all()
    for c in cours:
      slots=courses_slot.objects.filter(course_detail=c)
      for slot in slots:
        course_list[str(slot.slot%6)]['p'+str((slot.slot-1)//6)]=c.courses_name 
    for i in range(0,5):
        course_list[str(6)]['p'+str(i)]=course_list[str(0)]['p'+str(i)]
    del course_list[str(0)]
    context = {
      "course_dict" : course_list,
      "taken_courses" : cours
      }
    return render(request,"courses/myCourses.html",context)

@login_required(login_url="/login")
def delete(request):
  drop_id = request.POST['courses']
  p = request.user
  d = courses_detail.objects.get(pk=drop_id)
  p.courses_detail_set.remove(d)
  return redirect('courses')
