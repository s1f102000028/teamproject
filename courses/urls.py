from django.urls import path, include
from . import views
urlpatterns=[
    #/courses
    path('',include("django.contrib.auth.urls")),
    path('register',views.register,name="register"),
    path('',views.syllabus,name='syllabus'),
    path('overview', views.overview,name='overview'),
    path('<int:courses_id>/add',views.add, name="add"),
  #  path('<int:courses_id>/add',views.add,name='add'),
    path('courses',views.courses,name='courses'),
    path('delete',views.delete,name='delete'),
    #courses/courses_id
    #/courses/courses_detail/1 
    path('<int:courses_id>', views.course_detail,name='course_detail'),
]